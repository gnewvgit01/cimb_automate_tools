import React,{useState} from 'react'
import { Card, Row, Col, Form, Input, Button, } from 'antd'
import Logo from '../assets/img/automate_tools.jpg'
import MyHerder from '../Layouts/MyHerder';
import {Redirect} from 'react-router-dom'
interface Param {

}

type Props = Param
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};
const Login: React.FC<Props> = (props: Props) => {
    const [toListPage, setToListPage] = useState<boolean>(false)
    const onFinish = (values: any) => {
        console.log('Success:', values);
        setToListPage(true)
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div>
            {toListPage ? <Redirect to="/list"/> : null}
            <MyHerder/>
            <div className="site-card-border-less-wrapper">
                <Card bordered={false} className="bg-form" style={{ width: 500, borderRadius: 10 }}>
                    <Row>
                        <Col span={7} className="bg-img">
                            <img src={Logo} alt={'img'} height={'80%'} style={{marginTop: 17}}/>
                        </Col>
                        <Col span={17} className="bg-form" style={{padding: 5}}>
                            <Form
                                {...layout}
                                name="Login"
                                initialValues={undefined}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                            >
                                <Form.Item
                                    label="Username"
                                    name="username"
                                    rules={[{ required: true, message: 'Please input your username!' }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Password"
                                    name="password"
                                    rules={[{ required: true, message: 'Please input your password!' }]}
                                >
                                    <Input.Password />
                                </Form.Item>

                                <Form.Item {...tailLayout}>
                                    <Button htmlType="submit" style={{borderRadius: 10, backgroundColor: "#FF9E22"}}>
                                        login
                                </Button>
                                </Form.Item>
                            </Form>
                        </Col>
                    </Row>
                </Card>
            </div>
        </div>
    )
}

export default Login
