import React, {useState} from 'react'
import MyHerder from '../Layouts/MyHerder'
import { Row, Col, Button, Input } from 'antd'
import {
    SettingOutlined,
    FilterOutlined
  } from '@ant-design/icons';
import TableList from '../Components/TableList';
import MyForm from '../Components/MyForm';
interface Param {

}

type Props = Param
const { Search } = Input;
const List: React.FC<Props> = (props: Props) =>  {
    const [view, setView] = useState<number>(1)
    const onSearch = (value: string) => console.log(value);
    return (
        <div>
            <MyHerder/>
            <Row style={{backgroundColor: "#FFC77D", padding: 5}}>
                <Col span={23}>
                    <Button className="btn-sub-menu" onClick={() => setView(1)}>Show Tasks</Button>
                    <Button className="btn-sub-menu" onClick={() => setView(2)}>Provisioning VM</Button>
                    <Button className="btn-sub-menu" onClick={() => setView(3)}>Patching</Button>
                </Col>
                <Col span={1} style={{textAlign: 'end'}}>
                    <SettingOutlined style={{ fontSize: 32, color: '#000' }}/>
                </Col>
            </Row>
            {view === 1 ? <div className="div-list">
                <span className="font-mid">Search: </span>
                <Search placeholder="Input TaskId., ProjectName" allowClear onSearch={onSearch} style={{ width: 500, marginLeft: 25, marginBottom: 5 }} />
                <div className="font-mid">
                    <span><FilterOutlined /> Filter</span>
                </div>
                <br/>
                <TableList/>
            </div> : null}
            {view === 2 ? <div>
                <MyForm />
            </div> : null}
        </div>
    )
}

export default List
