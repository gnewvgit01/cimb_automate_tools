import React from 'react'

interface Param {

}

type Props = Param
const MyFooter: React.FC<Props> = (props: Props) => {
    return (
        <div className="Footer-Text"><span> G-Able Company Limited 1.0.3</span></div>
    )
}

export default MyFooter