import React from 'react'

interface Param {

}

type Props = Param
const MyHerder: React.FC<Props> = (props: Props) => {
    return (
        <div className="Herder-Text"><span> AUTOMATE TOOLS PORTAL</span></div>
    )
}

export default MyHerder
