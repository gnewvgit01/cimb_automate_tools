import React, { useState, useEffect } from 'react'
import { Form, Input, Button, Row, Col, Select, Radio, Slider, InputNumber, Modal } from 'antd';
import MyFooter from '../Layouts/MyFooter';
import { ExclamationCircleOutlined } from '@ant-design/icons';
interface Param {

}

type Props = Param
const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
};
const { Option } = Select;
const { confirm } = Modal;
const MyForm: React.FC<Props> = (props: Props) => {
    const [vCPU, setVCPU] = useState<number>(0)
    const [memory, setMemory] = useState<number>(0)
    const [disk1, setDisk1] = useState<number>(0)
    const [disk2, setDisk2] = useState<number>(0)
    const [copied, setCopied] = useState<number>(0)
    const [isShowDetail, setIsShowDetail] = useState<boolean>(false)
    const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
    const [HDTemplate, setHDTemplate] = useState<string>('')
    const [resultArray, setResultArray] = useState<any[]>([])
    const [isConfirm, setConfirm] = useState<boolean>(false)

    useEffect(() => {
        if (copied !== 0) {
            let temp = new Array(copied).fill(0)
            setResultArray(temp)
        }
    }, [copied])

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
        setConfirm(true)
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const onFinish = (values: any) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    const showConfirm = () => {
        return confirm({
          title: 'Create the VM?',
          icon: <ExclamationCircleOutlined />,
          content: 'Info: the last configuration is up to date..(not over 1 hour)',
          onOk() {
            console.log('OK');
          },
          onCancel() {
            console.log('Cancel');
          },
        });
      }

    return (
        <div>
            <Form
                {...layout}
                name="FormNaja"
                initialValues={{
                    'serverZone': 'DR_ServerCon',
                    'computeResource': 'comp_01.gable.com',
                    'os': 'Windows',
                    'Template': 'Z_Template_Window2019',
                    'StorageServer': 'Storage_Server_3',
                    'disk': 'thin',
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Row className="div-list" gutter={24} style={{ marginLeft: 12 }}>
                    <Col span={12}>
                        <Form.Item
                            label="Server Zone"
                            name="serverZone"
                        >
                            <Select
                                placeholder="Select a option and change input text above"
                                allowClear
                            >
                                <Option value="DR_ServerCon">DR_ServerCon</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item
                            label="Compute resource"
                            name="computeResource"
                        >
                            <Select
                                placeholder="Select a option and change input text above"
                                allowClear
                            >
                                <Option value="comp_01.gable.com">comp_01.gable.com</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item
                            label="Operating System"
                            name="os"
                        >
                            <Radio.Group >
                                <Radio.Button value="Linux">Linux</Radio.Button>
                                <Radio.Button value="Windows">Windows</Radio.Button>
                            </Radio.Group>
                        </Form.Item>

                        <Form.Item
                            label="VM Template"
                            name="Template"
                        >
                            <Select
                                placeholder="Select a option and change input text above"
                                allowClear
                            >
                                <Option value="Z_Template_Window2019">Z_Template_Window2019</Option>
                            </Select>
                        </Form.Item>

                        <Row gutter={16}>
                            <Col span={20}>
                                <Form.Item
                                    label="Storage Server"
                                    name="StorageServer"
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col span={4}>
                                <Button className="btn-fill" onClick={() => setIsShowDetail(!isShowDetail)}>Select</Button>
                            </Col>
                        </Row>

                        <Row className="showDetail">
                            <Col span={8}>Capacity : {!isShowDetail ? '-' : 4.66} TB</Col>
                            <Col span={8}>Provisioned : {!isShowDetail ? '-' : 1.71} TB</Col>
                            <Col span={8}>Free : {!isShowDetail ? '-' : 2.95} TB</Col>
                        </Row>
                        <br />
                        <Form.Item
                            label="Virtual disk format"
                            name="disk"
                        >
                            <Radio.Group >
                                <Radio.Button value="thin">thin</Radio.Button>
                                <Radio.Button value="thick">thick</Radio.Button>
                            </Radio.Group>
                        </Form.Item>

                        <Row>
                            <Col span={20}>
                                <Form.Item name="vCPU" label="vCPU">
                                    <Slider
                                        min={0}
                                        max={100}
                                        onChange={(e) => setVCPU(e)}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={4}> {vCPU} core</Col>
                        </Row>
                        <Row>
                            <Col span={20}>
                                <Form.Item name="Memory" label="Memory">
                                    <Slider
                                        min={0}
                                        max={100}
                                        onChange={(e) => setMemory(e)}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={4}> {memory} GB</Col>
                        </Row>
                        <Row>
                            <Col span={20}>
                                <Form.Item name="Disk1" label="Disk #1">
                                    <Slider
                                        min={0}
                                        max={100}
                                        onChange={(e) => setDisk1(e)}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={4}> {disk1} GB</Col>
                        </Row>
                        <Row>
                            <Col span={20}>
                                <Form.Item name="Disk2" label="Disk #2">
                                    <Slider
                                        min={0}
                                        max={100}
                                        onChange={(e) => setDisk2(e)}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={4}> {disk2} GB</Col>
                        </Row>

                    </Col>

                    <Col span={12}>
                        <Form.Item
                            label="Project Name"
                            name="ProjectName"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Copied"
                            name="Copied"
                        >
                            <InputNumber style={{ width: 80 }} min={0} onChange={(e: any) => setCopied(e)} />
                        </Form.Item>

                        <Row>
                            <Col span={12} style={{ textAlign: 'end', marginTop: 5 }}>
                                Guest IP Address / Server Name
                            </Col>
                            <Col span={12}>
                                <Button className={copied <= 0 && !isConfirm ? "btn-fill" : "btn-filled"} onClick={showModal}>fill value</Button>
                                <Modal title="Server Zone : DR_ServerCon" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={false}>
                                    <Row gutter={16}>
                                        <Col span={12}>
                                            <div style={{textAlign: 'center'}}>
                                                <p>Guest IP Address</p>
                                                {resultArray.map((res, index) => {
                                                    return (<Button className="btn-inshowdetail">192.168.1.10{index}</Button>)
                                                })}
                                            </div>
                                        </Col>
                                        <Col span={12}>
                                            <div style={{textAlign: 'center'}}>
                                                <p>VirtualMachine Name</p>
                                                {resultArray.map((res, index) => {
                                                    return (<Button className="btn-inshowdetail">VM_WEB_{index + 1}_UAT</Button>)
                                                })}
                                            </div>
                                        </Col>
                                    </Row>
                                    <br/>
                                    <div style={{textAlign: 'end'}}><Button className="btn-fill" onClick={handleOk}>Save Value</Button></div>
                                </Modal>
                            </Col>
                        </Row>
                        <br />
                        <Form.Item
                            label="DNS IP Address"
                            name="DNS"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Gateway IP Address"
                            name="Gateway"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Guest User Name"
                            name="UserName"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Guest Password"
                            name="Password"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="HD Template"
                            name="HDTemplate"
                        >
                            <Select
                                placeholder="Select a option and change input text above"
                                onChange={(e: any) => setHDTemplate(e)}
                                allowClear
                            >
                                <Option value="Z_2017_HardTemplate_Win">Z_2017_HardTemplate_Win</Option>
                            </Select>
                        </Form.Item>

                        <div className="showDetail2">
                            <div><span><b>Available Resources</b></span></div>
                            <div><span>Server Zone : {HDTemplate ? 'DR_ServerCon' : ''}</span></div>
                            <div><span>Total vCPU : {HDTemplate ? '24 core / 80 core' : ''} </span></div>
                            <div><span>Total Memory : {HDTemplate ? '128 GB / 5 TB' : ''} </span></div>
                            {HDTemplate ? <>
                                <br/>
                                <span style={{color: 'red', textAlign: 'center'}}>last updated on 25/2/2021 16:15:35</span>
                            </> : null}
                        </div>
                    </Col>
                </Row>

                <Form.Item >
                    <Button htmlType="submit" className="btn-end-page-position" onClick={showConfirm}>
                        Create VM
                    </Button>
                </Form.Item>
                <br />
            </Form>
            <MyFooter />
        </div>
    )
}

export default MyForm
