import React from 'react'
import { Table } from 'antd';
interface Param {

}

type Props = Param
const TableList: React.FC<Props> = (props: Props) => {
    const columns = [
        {
            title: 'Task ID',
            dataIndex: 'TaskID',
        },
        {
            title: 'ProjectName',
            dataIndex: 'ProjectName',
            sorter: {
                compare: (a: any, b: any) => a.chinese - b.chinese,
                multiple: 3,
            },
        },
        {
            title: 'Type',
            dataIndex: 'Type',
            sorter: {
                compare: (a: any, b: any) => a.math - b.math,
                multiple: 2,
            },
        },
        {
            title: 'Start',
            dataIndex: 'Start',
            sorter: {
                compare: (a: any, b: any) => a.english - b.english,
                multiple: 1,
            },
        },
        {
            title: 'End',
            dataIndex: 'End',
            sorter: {
                compare: (a: any, b: any) => a.english - b.english,
                multiple: 1,
            },
        },
        {
            title: 'Status',
            dataIndex: 'Status',
            sorter: {
                compare: (a: any, b: any) => a.english - b.english,
                multiple: 1,
            },
        },
    ];

    const data = [
        {
            key: '1',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '2',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '3',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '4',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '5',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '6',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '7',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '8',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '9',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '10',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '11',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
        {
            key: '12',
            TaskID: 'PT1000000001',
            ProjectName: 'Project Test _0010',
            Type: 'Provisioning VM',
            Start: '25/2/2021 17:30:55',
            End: '25/2/2021 18:20:23',
            Status: 'Completed'
        },
    ];

    return (
        <div>
            <Table columns={columns} dataSource={data} size="small" />
        </div>
    )
}

export default TableList
