import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Login from './Page/Login'
import List from './Page/List'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/list" component={List} />
      </Switch>
    </Router>
  );
}

export default App;
