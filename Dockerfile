# production environment
FROM nginx:1.16.0-alpine
ADD /build /var/www
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]